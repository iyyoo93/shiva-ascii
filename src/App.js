import { useState } from 'react';
import './App.css';

const initPixel = [
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
];

function App() {
  const [pixel, setPixel] = useState(initPixel);

  let a = '';

  pixel.map((rowPixel) =>
    rowPixel.map((colPixel) => {
      a += colPixel;
    })
  );

  const renderPixel = () => {
    let abc = [];
    for (let i = 0; i < 48; i = i + 8) {
      abc.push(
        <div className="rowPixel">
          <span>{a.substring(i, i + 8)} </span>===
          <span>{parseInt(a.substring(i, i + 8), 2)}</span>
        </div>
      );
    }
    return abc;
  };

  const handleClick = (row, col) => {
    const tmpPixel = [...pixel];
    tmpPixel[row][col] = tmpPixel[row][col] ? 0 : 1;
    setPixel(tmpPixel);
  };

  return (
    <div className="app">
      {pixel.map((rowPixel, rowIndex) => (
        <div className="row">
          {rowPixel.map((colPixel, colIndex) => (
            <button
              key={`${rowIndex}${colIndex}`}
              className={!colPixel && 'oneClass'}
              onClick={() => handleClick(rowIndex, colIndex)}
            >
              {colPixel}
            </button>
          ))}
        </div>
      ))}
      {renderPixel()}
      <button
        className="reset"
        onClick={() => {
          window.location.reload();
          // setPixel([...initPixel]);
        }}
      >
        Reset
      </button>
    </div>
  );
}

export default App;
